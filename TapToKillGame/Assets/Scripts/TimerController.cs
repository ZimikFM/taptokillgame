﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class TimerController : MonoBehaviour {

    [SerializeField]
    private float TimeOut;

    [SerializeField]
    private float TickInterval;

    [SerializeField]
    private UnityEvent TimerCompleteEvent;

    [SerializeField]
    private FloatChangeEvent TimerTickEvent;

    [SerializeField]
    private TextMeshPro TimerText;

    private float TimerValue;

    private float InternalTickInterval;

    public void ResetTimer()
    {
        TimerValue = TimeOut;
        InternalTickInterval = TickInterval;
    }

    public void StartTimer()
    {
        StartCoroutine(TimerTick());
    }

    private IEnumerator TimerTick()
    {

        TimerTickEvent.Invoke(TimerValue);
        UpdateTextField();
        yield return new WaitForSeconds(InternalTickInterval);
        TimerValue -= InternalTickInterval;
        InternalTickInterval = Mathf.Min(InternalTickInterval, TimerValue);
        if(TimerValue == 0)
        {
            TimerEnd();
        }
        else
        {
            StartCoroutine(TimerTick());
        }
        
    }

    private void TimerEnd()
    {
        UpdateTextField();
        TimerTickEvent.Invoke(TimerValue);
        TimerCompleteEvent.Invoke();
    }

    private void UpdateTextField()
    {
        long ticks = System.Convert.ToInt64(TimerValue * 10000000);
        DateTime dateTime = new DateTime(ticks);
        TimerText.SetText(String.Format("{0:ss.ff}", dateTime));
    }

    public void StopTimer()
    {
        StopAllCoroutines();
    }
}
