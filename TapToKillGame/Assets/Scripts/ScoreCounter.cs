﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreCounter : MonoBehaviour {

    [SerializeField]
    private TextMeshPro TextMeshPro;

    [SerializeField]
    private StringEvent ChangeScore;

    private int score;


    public void Reset()
    {
        Score = 0;
    }

    public void AddScore(int score)
    {
        Score += score;
    }

    private int Score
    {
        get
        {
            return score;
        }

        set
        {
            score = value;
            UpdateTextField();
        }
    }

    private void UpdateTextField()
    {
        ChangeScore.Invoke(Score.ToString());
        //TextMeshPro.SetText(Score.ToString());
    }


}
