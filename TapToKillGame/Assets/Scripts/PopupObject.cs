﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PopupObject : MonoBehaviour {

    [SerializeField]
    private float LifeTime;

    [SerializeField]
    private int ScorePoints;

    [SerializeField]
    private TapEvent TapEvent;

    // Use this for initialization
    void Start () {
 	}

    private void OnMouseDown()
    {
        TapEvent.Invoke(ScorePoints);
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update () {
		
	}


}
