﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupsController : MonoBehaviour {

    [SerializeField]
    private GameObject Container;

    [SerializeField]
    private PopupObject[] PopupPrefabs;

    [SerializeField]
    private BoxCollider2D BoxCollider;

    [SerializeField]
    private float GenerateInterval;

    private Coroutine currentCoroutine;

    private int PopupsWasGenerated = 0;

    public void StartGeneratePopups()
    {
        if(currentCoroutine == null)
        {
            currentCoroutine = StartCoroutine(GenerateLoop());
        }
    }

    public void StopGeneratePopups()
    {
        if (currentCoroutine != null)
        {
            StopCoroutine(currentCoroutine);
            currentCoroutine = null;
        }
    }

    private IEnumerator GenerateLoop()
    {
        while (true)
        {
            Generate();
            yield return new WaitForSeconds(GenerateInterval);
        }
    }

    private void Generate()
    {
        int itemIndex = Random.Range(0, PopupPrefabs.Length);
        GameObject templateObject = PopupPrefabs[itemIndex].gameObject;
        GameObject gm = Instantiate(templateObject, Container.transform);

        gm.transform.localPosition = new Vector3(Random.Range(0, BoxCollider.size.x), Random.Range(0, BoxCollider.size.y), -0.001f* PopupsWasGenerated);
        gm.SetActive(true);

        PopupsWasGenerated++;

    }
}
