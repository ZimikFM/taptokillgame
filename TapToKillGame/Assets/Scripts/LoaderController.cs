﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoaderController : MonoBehaviour {

    [SerializeField]
    private BaseAction LoadingCompleteAction;

	// Use this for initialization
	void Start () {
        StartLoading();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void StartLoading()
    {
        StartCoroutine(LoadingTestDelay());
    }

    private IEnumerator LoadingTestDelay()
    {
        yield return new WaitForSeconds(5);
        LoadingComplete();

    }

    private void LoadingComplete()
    {
        LoadingCompleteAction.DoAct();
    }
}
