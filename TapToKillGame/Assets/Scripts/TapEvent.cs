﻿using UnityEngine.Events;

[System.Serializable]
public class TapEvent : UnityEvent<int> {
}
