﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllChildrenRemoveAction : MonoBehaviour {

    [SerializeField]
    private Transform TransformValue;

    public void RemoveAllChildren()
    {
        while (TransformValue.childCount > 0)
        {
            DestroyImmediate(TransformValue.GetChild(0).gameObject);
        }
        
    }
}
