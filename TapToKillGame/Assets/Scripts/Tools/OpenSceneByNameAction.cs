﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class OpenSceneByNameAction : BaseAction
{
    [SerializeField]
    private string SceneName;

    public UnityEvent m_MyEvent;

    public override void DoAct()
    {
        SceneManager.LoadScene(SceneName);
    }

}
