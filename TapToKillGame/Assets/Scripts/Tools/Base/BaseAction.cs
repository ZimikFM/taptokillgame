﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class BaseAction : MonoBehaviour {

    abstract public void DoAct();
}
